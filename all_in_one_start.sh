#!/bin/bash

# 用于一键配置环境和编译运行 linux 0.01 的脚本
# 需要在 ubuntu的普通用户桌面打开图形终端，sudo su切换到root命令。然后执行本脚本。

# 安装编译器
if [ ! -d "/usr/share/doc/gcc-multilib/" ];then apt -y install  gcc build-essential  gcc-multilib;fi
# 安装 KVM 相关软件：
if [ ! -d "/usr/share/doc/qemu-kvm" ];then apt -y install bin86 qemu qemu-system-x86 qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils virtinst virt-manager;fi

#
if [ ! -d ".git" ];then git clone https://gitee.com/kjpioo2006/compilable-linux-0.x && cd compilable-linux-0.x;fi

if [ ! -f "./hd_oldlinux.img" ];then unzip hd_oldlinux.img.zip;fi
make
make run
# or 
# make run-curses 

# 修改 代码后，重新运行以上脚本，即可